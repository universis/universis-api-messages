/* eslint-disable no-unused-vars */
import {DataError, AbstractClassError, AbstractMethodError, ApplicationService, Args, IApplication} from '@themost/common';
import {DataContext} from '@themost/data';
import {SMS} from './index';


class InvalidMessageRecipient extends DataError {
	constructor(message, innerMessage) {
		super('E_RECIPIENT', message, innerMessage);
	}
}

class SmsService extends ApplicationService {
	/**
     * @param {IApplication} app
     */
	constructor(app) {
		super(app);
		if (this.constructor.name === 'SmsService') {
			throw new AbstractClassError();
		}
		this.install(app);
	}

	/**
	 * 
	 * @param {IApplication} app
	 */
	install(app) {
		if (app?.container) {
			let smsConfiguration;
			try {
				smsConfiguration = app.getConfiguration().getSourceAt('settings/universis/sms');
				if (!smsConfiguration) {
					throw new Error('SMSSettings are not defined');
				}
				Args.notNull(smsConfiguration?.serverUri, 'smsConfiguration.serverUri');
				Args.notNull(smsConfiguration?.endpoint, 'smsConfiguration.endpoint');
				Args.notNull(smsConfiguration?.balanceEndpoint, 'smsConfiguration.balanceEndpoint');
				Args.notNull(smsConfiguration?.deliveryEndpoint, 'smsConfiguration.deliveryEndpoint');
			} catch (err) {
				throw new Error(`Error while loading SMS service settings. ${err}`);
			}
		}
	}

	/**
	 * This uses the SMS provider API by creating a url based on the url and endpoint provided
	 * in application configuration, to send the message to the recipient.
	 * @abstract
	 * @param {DataContext} context
	 * @param {SMS} message
	 */
	async sendSMS(context, message) {
		throw new AbstractMethodError();
	}

	/**
	 * This uses the SMS provider API by creating a url based on the url and endpoint provided
	 * in application configuration, to check for the left balance in the institute account
	 * @abstract
	 */
	async checkBalance() {
		throw new AbstractMethodError();
	}

	/**
	 * This uses the SMS provider API by creating a url based on the url and endpoint provided
	 * in application configuration, to introspect the delivery status of the sms with the
	 * provider
	 * @abstract
	 * @param {SMS|string} message
	 */
	async checkDelivery(message) {
		throw new AbstractMethodError();
	}
}

export {
	SmsService,
	InvalidMessageRecipient
};

