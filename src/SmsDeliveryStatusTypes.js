export const SmsDeliveryStatusTypes = {
	'DELIVRD': 'SentSMSStatus',
	'ESME_ROK': 'ScheduledSMSStatus',
	'UNDELIV': 'CancelledSMSStatus',
	'REJECTD': 'FailedSMSStatus'
};
