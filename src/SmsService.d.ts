import {ApplicationService, DataError} from '@themost/common';
export declare class SmsService extends ApplicationService {
    sendSMS(context: any, message: any): Promise<any>;
    checkDelivery(message: any): Promise<any>;
    checkBalance(): Promise<any>;
}

export declare class InvalidMessageRecipient extends DataError {
    constructor(message: string, innerMessage?: string);
}