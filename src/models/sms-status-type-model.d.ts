import {EdmMapping, EdmType, DataObject} from '@themost/data';

/**
 * @class
 */
declare class SMSStatusType extends DataObject {

    public additionalType: string;
    public alternateName: string;
    public name: string;
    public identifier?: number;
    public id: number;
    public description?: string;
    public image?: string;
    public url?: string;
    public dateCreated?: Date;
    public dateModified?: Date;
    public createdBy?: any;
    public modifiedBy?: any;

}

export = SMSStatusType;
