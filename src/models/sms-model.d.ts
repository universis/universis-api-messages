import {EdmMapping, EdmType, DataObject} from '@themost/data';

/**
 * @class
 */
declare class SMS extends DataObject {

    public id: number;
    public body?: string;
    public sender?: any;
    public recipient?: any;
    public category?: string;
    public dateReceived?: Date;
    public owner?: any;
    public identifier?: string;
    public smsStatus?: any;
}

export = SMS;
